BEGIN {
    if (!length(color) || color == "auto") {
        if (system("[ -t 1 ]")) {
            color = 0
        } else {
            color = 1
        }
    } else if (color == "always") {
        color = 1
    } else if (color == "never") {
        color = 0
    } else {
        print "invalid value for color: " color >>"/dev/stderr"
        exit 1
    }

    cinit = 0

    CR       = "\033[m"
    C        = "\033[38;2;192;192;192m\033[22m\033[23m\033[24m"
    Cinit = CR "\033[48;2;000;000;000m" C

    Cwhite   = "\033[38;2;255;255;255m"

    Cpolicy  = "\033[38;2;192;224;255m"
    #Cappend  = Cwhite
    Ctable   = Cwhite

    Ciif     = "\033[38;2;192;255;192m"
    Csrc     = "\033[38;2;224;255;192m"
    Csport   = "\033[38;2;064;255;192m"

    Coif     = "\033[38;2;255;192;192m"
    Cdest    = "\033[38;2;255;224;192m"
    Cdport   = "\033[38;2;255;064;192m"

    Cproto   = "\033[38;2;128;128;255m"
    Cmodule  = "\033[38;2;000;000;255m"
    Cstate   = "\033[38;2;192;000;255m"
    Ccomment = "\033[38;2;064;064;064m"
    Clog     = C

    Cchain   = "\033[38;2;000;192;255m"
    Caccept  = "\033[38;2;000;255;000m"
    Creject  = "\033[38;2;255;128;000m"
    Cdrop    = "\033[38;2;255;000;000m"
    Ctarget  = Cwhite

    Cnot     = "\033[38;2;255;255;000m"

    #Cip      = "\033[1m\033[4m"
    #CipOFF   = "\033[22m\033[24m"
    Cip      = "\033[1m"
    CipOFF   = "\033[22m"
    #Cip      = "\033[4m"
    #CipOFF   = "\033[24m"
    #Cip      = "\033[3m"
    #CipOFF   = "\033[23m"
    #Cip      = ""
    #CipOFF   = ""

    #https://stackoverflow.com/questions/53497/regular-expression-that-matches-valid-ipv6-addresses
    IPv6 = "([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])(/(12[0-8]|1[0-1][0-9]|[0-9][0-9]|[0-9]))?"

    IPv4 = "((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])(/(3[012]|[21][0-9]|[0-9]))?"
}

function policy(line) {
    if (!color) {
        print line
        return
    }

    line = gensub("(^|\\s)(-t\\s+\\S+)(\\s|$)", "\\1" Ctable "\\2" C "\\3", 1, line)

    _line = gensub("(^|\\s)(-P\\s+\\S+)(\\s+)(ACCEPT)(\\s|$)",       "\\1" Cpolicy "\\2" C "\\3" Caccept "\\4" C "\\5", 1, line)
    if (_line == line) {
        _line = gensub("(^|\\s)(-P\\s+\\S+)(\\s+)(REJECT)(\\s|$)",   "\\1" Cpolicy "\\2" C "\\3" Creject "\\4" C "\\5", 1, line)
        if (_line == line) {
            _line = gensub("(^|\\s)(-P\\s+\\S+)(\\s+)(DROP)(\\s|$)", "\\1" Cpolicy "\\2" C "\\3" Cdrop   "\\4" C "\\5", 1, line) }}

    if (cinit) {
        print _line
    } else {
        cinit = 1
        print Cinit _line
    }
}

function rule(line) {
    if (!color) {
        print line
        return
    }

    line = gensub(IPv6 "|" IPv4, Cip "&" CipOFF, "g", line)

    line = gensub("(^|\\s)(-m\\s+comment\\s+--comment\\s+(\"[^\"]*\"|\\S+))(\\s|$)",
                                                              "\\1" Ccomment "\\2" C "\\4", 1,   line)

    line = gensub("(^|\\s)(-A\\s+\\S+)(\\s|$)",               "\\1" Cchain   "\\2" C "\\3", 1,   line)

    line = gensub("(^|\\s)(-t\\s+\\S+)(\\s|$)",               "\\1" Ctable   "\\2" C "\\3", 1,   line)

    line = gensub("(^|\\s)(!)(\\s|$)",                        "\\1" Cnot     "\\2" C "\\3", "g",  line)
    line = gensub("(^|\\s)(-i\\s+\\S+)(\\s|$)",               "\\1" Ciif     "\\2" C "\\3", 1,   line)
    line = gensub("(^|\\s)(-o\\s+\\S+)(\\s|$)",               "\\1" Coif     "\\2" C "\\3", 1,   line)
    line = gensub("(^|\\s)(-s\\s+\\S+)(\\s|$)",               "\\1" Csrc     "\\2" C "\\3", 1,   line)
    line = gensub("(^|\\s)(-d\\s+\\S+)(\\s|$)",               "\\1" Cdest    "\\2" C "\\3", 1,   line)
    line = gensub("(^|\\s)(-m\\s+\\S+)(\\s|$)",               "\\1" Cmodule  "\\2" C "\\3", "g", line)
    line = gensub("(^|\\s)(-p\\s+\\S+)(\\s|$)",               "\\1" Cproto   "\\2" C "\\3", 1,   line)
    line = gensub("(^|\\s)(--sports?\\s+\\S+)(\\s|$)",        "\\1" Csport   "\\2" C "\\3", "g", line)
    line = gensub("(^|\\s)(--dports?\\s+\\S+)(\\s|$)",        "\\1" Cdport   "\\2" C "\\3", "g", line)

    line = gensub("(^|\\s)(--(ct)?state\\s+\\S+)(\\s|$)",     "\\1" Cstate   "\\2" C "\\4", 1,   line)

    if (!tchains[table]) {
        tchains[table] = 1
        pchains = ""
        _separator = ""
        for (i=0; i<nchains; i++) {
            pchains = pchains _separator chains[i]
            _separator = "|"
        }
    }

    if (nchains) {
        _line = gensub("(^|\\s)(-j\\s+(" pchains "))(\\s|$)", "\\1" Cchain   "\\2" C "\\4", 1, line)
    } else {
        _line = line
    }
    if (_line == line) {
        _line = gensub("(^|\\s)(-j\\s+ACCEPT)(\\s|$)",        "\\1" Caccept  "\\2" C "\\3", 1, line)
        if (_line == line) {
            _line = gensub("(^|\\s)(-j\\s+REJECT)(\\s|$)",    "\\1" Creject  "\\2" C "\\3", 1, line)
            if (_line == line) {
                _line = gensub("(^|\\s)(-j\\s+DROP)(\\s|$)",  "\\1" Cdrop    "\\2" C "\\3", 1, line)
                if (_line == line) {
                    _line = gensub("(^|\\s)(-j\\s+LOG(\\s+--log-prefix\\s+(\"[^\"]*\"|\\S+))?)(\\s|$)",
                                                              "\\1" Clog     "\\2" C "\\5", 1, line)
                    if (_line == line) {
                        _line = gensub("(^|\\s)(-j\\s+\\S+)(\\s|$)",
                                                              "\\1" Ctarget  "\\2" C "\\3", 1, line) }}}}}

    if (cinit) {
        print _line CR
    } else {
        cinit = 1
        print Cinit _line CR
    }
}

########################################

/^(#|(COMMIT)?\s*$)/ {
    next
}

/^\*\S+\s*$/ {
    table = substr($1,2)
    nchains = 0
    next
}

/^:\S+\s+\S+(\s+\[[0-9]+:[0-9]+\])?\s*$/ {
    if (table == "") table="NA"
    chain = substr($1,2)
    pol = $2
    chains[nchains++] = chain
    #if (pol != "-")
    policy(sprintf("-t %-8s -P %-29s %s", table, chain, pol))
    next
}

/^-A\s+\S+\s+\S+\s+\S/ {
    if (table == "") table="NA"
    action=$1
    chain=$2
    rule(sprintf("-t %-8s %s %-29s %s", table, action, chain,
                 gensub("^-A\\s+\\S+\\s+(.*)\\s*$", "\\1", 1)))
    next
}

########################################

{
    print "invalid line: " gensub("\\s*$", "", 1) >>"/dev/stderr"
    exit 1
}
